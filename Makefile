
VERSION = 2.0

SCRIPTS = $(CURDIR)/ipfireboot.ipxe
MACHINE = $(shell uname -m)

TARGETS = bin/ipxe.iso bin/ipxe.kpxe bin/undionly.kpxe bin/ipxe.dsk \
	bin/ipxe.usb bin/ipxe.lkrn

ifeq "$(MACHINE)" "x86_64"
	TARGETS += bin-x86_64-efi/ipxe.efi
endif

.PHONY: all
all: $(TARGETS)

clean:
	rm -rfv $(SCRIPTS) $(TARGETS)
	make -C ipxe/src clean veryclean

%: %.in
	sed -e "s/@VERSION@/$(VERSION)/g" \
		< $< > $@

$(TARGETS): $(SCRIPTS) config/branding.h config/general.h
	# Copy our configuration to the gPXE submodule
	cp -vf config/* ipxe/src/config/local/

	# Build the image
	make -C ipxe/src EMBED=$(SCRIPTS) NO_WERROR=1 \
		ISOLINUX_BIN=/usr/share/syslinux/isolinux.bin $@

	# Copy the image to bin/
	-mkdir -pv $$(dirname $@)
	cp -vf ipxe/src/$@ $@
