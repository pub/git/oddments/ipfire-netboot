
// Enable support for syslinux's com32 images.
//#define IMAGE_COMBOOT

// Enable HTTPS downloads
#define DOWNLOAD_PROTO_HTTPS

// Disable iSCSI, AoE, Infiniband SCSI RDMA protocol and FibreChannel
#undef SANBOOT_PROTO_ISCSI
#undef SANBOOT_PROTO_AOE
#undef SANBOOT_PROTO_IB_SRP
#undef SANBOOT_PROTO_FCP
